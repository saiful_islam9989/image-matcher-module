import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;        
 
class Image {
		
	public double getResult(String inFile, String templateFile){
		
		Mat img = Highgui.imread(inFile);
        Mat templ = Highgui.imread(templateFile);
        
        Imgproc.cvtColor(img, img, Imgproc.COLOR_RGBA2GRAY);

        Imgproc.cvtColor(templ, templ, Imgproc.COLOR_RGBA2GRAY);

        img.convertTo(img, CvType.CV_32F);
        templ.convertTo(templ, CvType.CV_32F);

        Mat hist1 = new Mat();
        Mat hist2 = new Mat();

        MatOfInt histSize = new MatOfInt(180);
        MatOfInt channels = new MatOfInt(0);

        ArrayList<Mat> bgr_planes1 = new ArrayList<Mat>();
        ArrayList<Mat> bgr_planes2 = new ArrayList<Mat>();

        Core.split(img, bgr_planes1);
        Core.split(templ, bgr_planes2);//

        MatOfFloat histRanges = new MatOfFloat(0f, 180f);
        boolean accumulate = false;

        Imgproc.calcHist(bgr_planes1, channels, new Mat(), hist1,
                histSize, histRanges, accumulate);
        Core.normalize(hist1, hist1, 0, hist1.rows(),
                Core.NORM_MINMAX, -1, new Mat());

        Imgproc.calcHist(bgr_planes2, channels, new Mat(), hist2,
                histSize, histRanges, accumulate);
        Core.normalize(hist2, hist2, 0, hist2.rows(),
                Core.NORM_MINMAX, -1, new Mat());

        hist1.convertTo(hist1, CvType.CV_32F);
        hist2.convertTo(hist2, CvType.CV_32F);

        double compare = Imgproc.compareHist(hist1, hist2,
                Imgproc.CV_COMP_CORREL);
        System.out.println("comp val:"+ compare);
        return compare;
	}
}

public class ImageResult{
    public static void main (String args[]){
    	
    	
    	System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    	Image ob = new Image();
    	ob.getResult("out.png", "unpic.png");
    	
    }
}
